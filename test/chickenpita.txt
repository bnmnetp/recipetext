Marinated Chicken Pita Sandwich

By: DAVID SANDFORD 
"This is a very tasty and extremely simple sandwich. You do need to marinate the chicken at least 3 hours. I usually do it overnight."
 Rate/Review | Read Reviews (25)
1,675 people have saved this | 0 custom versions
Add to Recipe Box
Add to Shopping List
Print this Recipe
Share/Email
 
Customize Recipe
Kitchen-friendly View
What to Drink?

	Sangiovese
	Beer
Prep Time:
10 Min Cook Time:
10 Min Ready In:
3 Hrs 20 Min
Servings  (Help)

USMetricCalculate
 
Original Recipe Yield 2 sandwiches  
Ingredients

1/4 cup olive oil
1/4 cup lemon juice
1/2 teaspoon salt
1/2 teaspoon dried oregano
1/4 teaspoon garlic powder
1/8 teaspoon pepper
1 boneless, skinless chicken breast, cubed
 
1 pocket bread round, cut in half
1 small onion, sliced
1 tomato, sliced
1/2 cup plain yogurt
Directions

Combine the olive oil, lemon juice, salt, oregano, garlic powder, and pepper in a large resealable bag. Add chicken cubes, mix well, and allow to marinate in the refrigerator 3 hours or overnight.
Pour the chicken and marinade into a frying pan over medium-high heat. Cook until the chicken is no longer pink. Stuff each half of the pocket bread with chicken, onion, tomato, and yogurt; serve while still hot.
