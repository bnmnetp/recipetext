Beef Tenderloin Medallions with Potato "Risotto"
Bon Appétit  | February 2010
by Bruce Aidells
recipe
reviews (22)
photo
video
my notes
find out more
user rating
100% would make it again

user rating: 
Beef Tenderloin Medallions with Potato "Risotto"
4 forks
rate this recipe review this recipe
at a glance
main ingredients
Beef Tenderloin,  Root Vegetable,  Vegetable,  Meat,  Potato,  Beef
more resources

food dictionary
cooking videos
 enlarge image
yield: 6 servings
Potatoes are finely cut into small cubes, then cooked risotto-style.

subscribe to Bon Appétit
ingredients

1 tablespoon butter
1 cup finely chopped onion
1 pound Yukon Gold potatoes, peeled, cut into 1/8-inch-thick slices, then 1/8-inch cubes
1/8 teaspoon cayenne pepper
Coarse kosher salt
1 1/2 cups (or more) low-salt chicken broth
1/2 cup heavy whipping cream
1/4 cup finely grated Parmesan cheese
1/4 cup finely chopped fresh chives
6 6-ounce beef tenderloin steaks (each 3/4 to 1 inch thick)
1 1/2 tablespoons chopped fresh thyme
1 tablespoon (or more) olive oil
print a shopping list for this recipe

preparation

Melt butter in heavy large saucepan over medium heat. Add onion; cover and cook until soft and translucent, stirring occasionally, about 7 minutes. Add potato cubes and cayenne pepper; sprinkle with coarse salt and pepper. Add 1 1/2 cups chicken broth; bring to boil. Reduce heat to medium and simmer until potatoes are almost tender, adding more chicken broth by tablespoonfuls if dry (mixture should be creamy with some sauce, not dry), about 8 minutes. Add cream and simmer until potatoes are tender but still hold their shape, stirring often, about 10 minutes. Stir in cheese and chives. DO AHEAD: Can be made 2 hours ahead. Let stand at room temperature. Rewarm over medium heat, stirring often.
Sprinkle steaks with coarse salt and pepper, then chopped thyme. Heat 1 tablespoon olive oil in heavy large skillet over medium-high heat. Add steaks and cook to desired doneness, adding more oil as needed, 3 to 4 minutes per side for medium-rare.
Divide potato risotto among plates. Place steaks on plates and serve.


Read More http://www.epicurious.com/recipes/food/views/Beef-Tenderloin-Medallions-with-Potato-Risotto-357259#ixzz0xoGzqDDI
