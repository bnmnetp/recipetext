#!/bin/zsh

for i in *.txt; do
   echo $i
   python2.5 ../src/recipe_dissect.py $i >| ../testresult/$i
done

cd ../expect
for i in *.txt; do
   echo $i
   diff $i ../testresult/$i
done
