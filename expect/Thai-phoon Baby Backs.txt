Thai-Phoon Baby Backs

INGREDIENTS:

- 1 1/2 cups pineapple, crushed
- 2 tablespoons fish sauce
- 1/4 cup lime juice
- 3 cloves garlic
- 1 1/2 stalk lemon grass
- 2 slabs baby back ribs
- 2 tablespoons peanut oil
- 1/2  stalk lemon grass
- 2 cloves garlic
- 1 cup pineapple, crushed
- 3/4 cup cider vinegar
- 2 tablespoons fish sauce
- 2 teaspoons red pepper flakes
- 2 tablespoons cilantro


METHOD:

At least two hours, and prefereably the night before make the marinade
by pureeing the marinade ingredients together.


SERVINGS: 8
SOURCE: Smoke & Spice
DIFFICULTY: Easy
PREP TIME: 20 minutes
RATING: 4
TAGS: grill
NOTES: 


