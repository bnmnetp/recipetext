import os, glob, os.path, codecs, logging, cgi, urllib
from datetime import datetime

import base64
import Cookie
import email.utils
import hashlib
import hmac
import time
import wsgiref.handlers
import facebook

from django.utils import simplejson as json
from google.appengine.ext import webapp
from google.appengine.ext.webapp import util
from google.appengine.api import users
from google.appengine.ext.db import Key
from google.appengine.ext.webapp import template
from google.appengine.api import memcache
from myutil import *
from model import Recipe, FBUser
from recipe_dissect import ParsedRecipe

FACEBOOK_APP_ID = "71806252102"
FACEBOOK_APP_SECRET = "19f8f6573ea279ef602db755f2a101b5"


class APIHandler(webapp.RequestHandler):
    @property
    def current_user(self):
        """Returns the logged in Facebook user, or None if unconnected."""
        if not hasattr(self, "_current_user"):
            self._current_user = None
            user_id = self.uid_from_path(self.request.path)
            if user_id:
                self._current_user = FBUser.get_by_key_name(user_id)
        return self._current_user

    def create_context(self):
        cd = {}

        if self.current_user:
            cd['nickname'] = self.current_user.name



    def uid_from_path(self,path):
        """
        API URL's are of the form /api/uid/command/args
        """
        path_parts = path.split('/')
        uid = path_parts[2].strip()
        return uid

