#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import os, glob, os.path, codecs, logging, cgi, re
from datetime import datetime

from google.appengine.ext import webapp
from google.appengine.ext.webapp import util
from google.appengine.api import users
from google.appengine.ext.db import Key
from google.appengine.api import memcache

from recipe_dissect import ParsedRecipe, ImportedRecipe

from model import Recipe, FBUser, Tag

from apibase import APIHandler

# REST services
# put these lines here as they only need to be executed once.
#
import rest
rest.Dispatcher.base_url = '/rest'
rest.Dispatcher.add_models_from_module(__name__)


class ListRecipes(APIHandler):
  """
  In response to /getlist
  There are three options:
      /getlist/title
      /getlist/key
      /getlist/both
  """
  def get(self):

    parms = self.request.path.split('/')
    logging.debug("path is: " + self.request.path)
    command = parms[-1]
    if self.current_user:
        rlist = self.current_user.recipes
    else:
        rlist = []

    
    self.response.headers['Content-Type'] = "text/xml; charset=utf-8"
    self.response.out.write('''<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>
''')

    for rkey in rlist:
      logging.debug("top of loop")
      if command == "key" or command == "both":
          self.response.out.write('<string>'+ str(rkey) + '</string>\n')
      if command == "title" or command == "both":
          r = Recipe.get(rkey)
          logging.debug(r.title)
          self.response.out.write('<string>'+cgi.escape(r.title) + '</string>\n')

    self.response.out.write('</array>\n')
    self.response.out.write('</plist>\n')

    
class GetRecipeText(APIHandler):
  """
  In response to /api/uid/getrecipe
  parameters are:
      /getrecipe/<google recipe key>
  """
  
  def get(self):
    """
    
    Arguments:
    - `self`:
    """
    parms = self.request.path.split('/')
    recipe_id = parms[-1]
    
    recipe = Recipe.get(Key(recipe_id))
    if recipe.public or self.current_user.key() in recipe.cooks: 
        self.response.out.write(recipe.recipeText)
    else:
        self.response.out.write("User %s not authorized for recipe %s"%(self.current_user.key(),recipe.key()))
    
    
class SyncRecipes(APIHandler):
  """
  In response to /sync
  There are three options:
      the dates are not handled as url parameters..
  """
  def get(self):

    parms = self.request.path.split('/')
    lastupdate = self.request.get('lastsync')
    lastupdate = datetime.strptime(lastupdate,"%Y-%m-%d %H:%M:%S")
    me = self.current_user
    if me:
        logging.debug("looking for recipes modified after "+str(lastupdate))
        rlist = Recipe.gql("WHERE cooks = :1 and last_update > :2", me.key(),lastupdate)
        logging.debug("query result = " + str(rlist))
        self.response.headers['Content-Type'] = "text/xml; charset=utf-8"
        self.response.out.write('''<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>
''')

        for r in rlist:
            self.response.out.write('<string>'+ str(r.key()) + '</string>\n')
            self.response.out.write('<string>'+cgi.escape(r.title.strip()) + '</string>\n')
            self.response.out.write('<string>'+cgi.escape( str(r.last_update)) + '</string>\n')        

        self.response.out.write('</array>\n')
        self.response.out.write('</plist>\n')
    else:
        raise ValueError("No Valid User Provided")


class BulkImporter(APIHandler):

    def post(self):
        recipeText = self.request.get('import_text')
        myRecipe = ParsedRecipe(recipeText)
        newRec = Recipe()
        me = self.current_user
        if not me:
            raise ValueError("Bad User Id")
        newRec.submitted_by = me
        newRec.recipeText = unicode(myRecipe)
        newRec.title = myRecipe.name
        newRec.last_update = datetime.now()
        newRec.public = True
        newRec.cooks.append(me.key())
        recipeTextList = recipeText.split('\n')
        for l in recipeTextList:
            if l.find('TAGS:') == 0:  # 3.0 incompatibility
                ts = l[5:]
                tlist = ts.split(',')
                if tlist:
                    for t in tlist:
                        st = t.strip()
                        newRec.taglist.append(st)
                        tagQuery = Tag.gql('where tag = :1',st)
                        if tagQuery.count() == 0:
                            newTag = Tag()
                            newTag.tag = st
                            newTag.put()


        newRec.put()
        me.recipes.append(newRec.key())
        me.put()


    
def main():
  logging.getLogger().setLevel(logging.DEBUG)
#  logging.debug(str(os.environ))
  application = webapp.WSGIApplication([('/api/.*/getlist/.*', ListRecipes),
                                        ('/api/.*/sync', SyncRecipes),
                                        ('/api/.*/getrecipe/.*',GetRecipeText),
                                        ('/api/.*/putrecipe', BulkImporter),                                        
                                        ('/rest/.*', rest.Dispatcher),
                                        ],
                                       debug=True)

  util.run_wsgi_app(application)


if __name__ == '__main__':
  main()
