# RecipeText #

This documents the RecipeText code as of October 2010.  I'm going to call this version 2 when I push it to appspot.

License:  All of the code in this project is copyright 2010 Brad Miller


## Introduction ##



## RecipeWeb ##

The `recipeweb.py` file contains all of the handlers for the website recipepad.appspot.com.

* Interactive import
* recipe index
* tag list
* recipe editing
* mark recipe public
* add/remove a recipe from your collection
* search recipes

### Current Limitations ###

* Need to add a way to share a recipe back to Facebook
* Need some way to generate a good public URL for the user.  This URL should be auto shared back to facebook.

## API ##

The `api.py` file contains all of the handlers for the API.  This API is designed to work with the iPad application, but it should be open and simple enough to use for anyone that wants to build an app.

### Current Limitations ###

* There is no way to initialize a user using the API today.  This would not be too hard, we would just need to pass along an access token somehow.

