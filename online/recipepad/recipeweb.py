import os, glob, os.path, codecs, logging, cgi, urllib
from datetime import datetime

import base64
import Cookie
import email.utils
import hashlib
import hmac
import time
import wsgiref.handlers
import facebook
import os

from google.appengine.ext.webapp import util
from google.appengine.api import users
from google.appengine.ext.db import Key
from google.appengine.ext.webapp import template
from google.appengine.api import memcache
import webapp2

from myutil import *
from model import Recipe, FBUser, Tag
from recipe_dissect import ParsedRecipe, ImportedRecipe

from basehandler import *


def genTagList(rlist,me):
    if me:
        ckey = "taglist_"+me.profile_url
    else:
        ckey = "taglist_"+"public"  # TODO fix this we don't want all public requests the same
    tagList = memcache.get(ckey)
    logging.debug("taglist = " + str(tagList))
    if not tagList:
        logging.debug('no cached tags found for ' + ckey)
        tagList = {}
        for r in rlist:
            for t in r.taglist:
                if t not in tagList:
                    tagList[t] = 1
                else:
                    tagList[t] += 1
        tagList = tagList.items()
        tagList.sort(lambda x, y: cmp(y[1],x[1]))
        stat = memcache.add(ckey,tagList,1800)
    return tagList

def getCurrentRecipe(user, recipe_key):
    """
    
    Arguments:
    - `user`:
    - `recipe_key`:
    """
    ckey ='recipe_'+user.profile_url
    recipe = memcache.get(ckey)
    if not recipe:
        recipe = Recipe.get(Key(recipe_key))
        memcache.add(ckey,recipe,300)

    return recipe

class Home(BaseHandler):
    def get(self):
        path = os.path.join('templates','index.html')
        self.response.out.write(template.render(path,self.create_context()))

class Settings(BaseHandler):
    def get(self):
        if not self.current_user:
            self.redirect("/auth/login")
        path = os.path.join('templates','settings.html')
        self.response.out.write(template.render(path,self.create_context()))


class SettingsSave(BaseHandler):
    def get(self):
        new_public_link = self.request.get('nickname')
        if self.current_user:
            checklist = FBUser.all().filter("public_link = ",new_public_link)
            if checklist.count() == 0:
                self.current_user.public_link = new_public_link
                self.current_user.put()
                self.redirect("/myindex")
            else:
                cd = self.create_context()
                cd['errormess'] = "Sorry but, %s is already taken, please choose another" % new_public_link
                path = os.path.join('templates','settings.html')
                self.response.out.write(template.render(path,cd))
        else:
            self.redirect("/auth/login")



class Help(BaseHandler):
    def get(self):
        path = os.path.join('templates','help.html')
        self.response.out.write(template.render(path,self.create_context()))

    
class RecipeIndex(BaseHandler):
    """
    Create an index of all recipes.  Each recipe should be clickable
    Create a tag cloud for all recipes too
    """
    
    def get(self):
        me = self.current_user
        if not me:
            self.redirect("/auth/login?lasthop=/myindex")
            return
        ckey = "rlist_"+me.profile_url
        rlist = memcache.get(ckey)
        if not rlist:
            logging.debug('no cached recipes found for ' + ckey)
            rlist = Recipe.gql("where cooks = :1 order by title", me.key())
            memcache.add(ckey,rlist,1800)
        path = os.path.join('templates','recipelist.html')
        tagList = genTagList(rlist,me)
        cd = self.create_context()
        cd['recipes'] = rlist
        cd['tags'] = tagList
        self.response.out.write(template.render(path,cd))


class PublicIndex(BaseHandler):
    '''
    Responds to /public/<nickname>
    '''
    def get(self):
        parms = self.request.path.split('/')
        nickname = urllib.unquote(parms[-1])
        logging.debug("the nickname is: " + nickname)
        cooks = FBUser.gql("where public_link = :1",nickname)
        try:
            theUser = cooks[0]
        except:
            self.redirect("/")   # TODO make this an error page.

        rlist = Recipe.gql("where __key__ in :1", theUser.recipes)
            
        path = os.path.join('templates','recipelist.html')
        tagList = genTagList(rlist,theUser)
        cd = self.create_context()
        cd['recipes'] = rlist
        cd['tags'] = tagList
        self.response.out.write(template.render(path,cd))
            

class RecipeDetail(BaseHandler):

    def get(self):
        if 'HTTP_REFERER' in os.environ:
            refer = os.environ['HTTP_REFERER']
            logging.debug('coming from ' + os.environ['HTTP_REFERER'])
        else:
            logging.debug('no referring data')
        parms = self.request.path.split('/')
        recipe_key = parms[-1]
        recipe = Recipe.get(Key(recipe_key))
        if recipe.public == False and not self.current_user:
            self.redirect("/auth/login")
            return
        if self.current_user:
            memcache.set('recipe_'+self.current_user.profile_url,recipe,300)
            
        if self.current_user.key() not in recipe.cooks:
            addButton = True
        else:
            addButton = False
        recipeText = recipe.recipeText
        myRec = ParsedRecipe(recipeText)

        path = os.path.join('templates','detail.html')
        ctx = self.create_context()
        rd = {}
        rd['name'] = myRec.name
        rd['ingredients'] = myRec.htmlIngredients()
        rd['method'] = myRec.htmlMethod()
        rd['notes'] = myRec.htmlNotes()
        rd['info'] = myRec.htmlInfo()
        rd['public'] = recipe.public
        rd['key'] = str(recipe.key())
        rd['add'] = addButton
        ctx['recipe'] = rd
        rhtml = template.render(path,ctx)
        self.response.out.write(rhtml)


class TagList(webapp2.RequestHandler):

    def get(self):
        """
        
        Arguments:
        - `self`:
        """
        parms = self.request.path.split('/')
        tag = parms[-1]
        rlist = Recipe.gql('where taglist = :1',tag)
        tagList = genTagList(rlist,None)
        path = os.path.join('templates','recipelist.html')
        cd = createContext()
        cd['recipes'] = rlist
        cd['tags'] = tagList
        self.response.out.write(template.render(path,cd))


class RecipeSearch(BaseHandler):

    def post(self):
        keys = self.request.get('keywords')
        logging.debug('search keys = ' + keys)
        #rlist = Recipe.all().search(keys,properties=['recipeText']).order('title')
        rlist = Recipe.search(keys)
        tagList = genTagList(rlist,None)
        path = os.path.join('templates','recipelist.html')
        cd = self.create_context()
        cd['recipes'] = rlist
        cd['tags'] = tagList
        self.response.out.write(template.render(path,cd))


class RecipeEdit(BaseHandler):
    def get(self):
        user = self.current_user
        if not user:
            self.redirect("/auth/login?lasthop=/myindex")

        parms = self.request.path.split('/')
        recipe_key = parms[-1]
        recipe = getCurrentRecipe(user,recipe_key)
        recipeText = recipe.recipeText

        path = os.path.join('templates','editrecipe.html')
        cd = createContext()
        cd['recipetext'] = recipeText
        cd['key'] = recipe_key
        self.response.out.write(template.render(path,cd))

class SaveEdit(BaseHandler):
    def post(self):
        user = self.current_user
        recipe_key = self.request.path.split('/')[-1]
        recipe = getCurrentRecipe(user,recipe_key)
        newText = self.request.get('recipe_text')
        recipe.recipeText = newText
        recipe.title = newText[:newText.index('\n')].strip()
        recipe.last_update = datetime.now()
        recipe.save()
        memcache.set('recipe_'+user.profile_url,recipe,300)
        memcache.delete_multi(["rlist_"+user.profile_url, "taglist_"+user.profile_url])  # invalidate rlist and tag caches

        self.redirect("/showrecipe/"+str(recipe.key()))
        

    
class ChangePublic(BaseHandler):
    def get(self):
        recipe_key = self.request.get('key').strip()
        logging.debug('key = ' + recipe_key )
        status = self.request.get('public')
        ckey = 'recipe_'+self.current_user.profile_url
        recipe = memcache.get(ckey)
        if not recipe:
            recipe = Recipe.get(Key(recipe_key))
        if status == 'true':
            recipe.public = True
        else:
            recipe.public = False
        recipe.put()
        self.response.out.write('success')

class AddRemove(BaseHandler):
    def get(self):
        recipe_key = self.request.get('key').strip()
        logging.debug('key = ' + recipe_key )
        add = self.request.get('add')
        user = self.current_user
        ckey = 'recipe_'+self.current_user.profile_url
        recipe = memcache.get(ckey)

        if not recipe:
            recipe = Recipe.get(Key(recipe_key))
        if add == 'true':
            recipe.cooks.append(self.current_user.key())
        else:
            recipe.cooks.remove(self.current_user.key())
        recipe.put()
        memcache.set(recipe_key,recipe,300)
        memcache.delete_multi(["rlist_"+user.profile_url, "taglist_"+user.profile_url])  # invalidate rlist and tag caches

        self.response.out.write('success')



class ImportHandler(BaseHandler):
    """
    1. Read recipe text from form
    2. Process recipe with import logic
    3. show imported recipe with button to accept and store, or to edit.
    """
    
    def post(self):
        recipeText = self.request.get('import_text')
        myRecipe = ImportedRecipe(recipeText)
        path = os.path.join('templates','newrecipe.html')
        cd = self.create_context()
        cd['recipetext'] = unicode(myRecipe)
        cd['originaltext'] = recipeText
        self.response.out.write(template.render(path,cd))
        
        
class RecipeSaver(BaseHandler):

    def post(self):
        recipeText = self.request.get('new_recipe')
        title = recipeText[:recipeText.index('\n')]
        me = self.current_user

        
        newRec = Recipe()
        newRec.submitted_by = me
        newRec.title = title.strip()
        newRec.recipeText = "".join(recipeText)
        newRec.last_update = datetime.now()
        newRec.cooks.append(me.key())
        newRec.public = True
        recipeTextList = recipeText.split('\n')
        for l in recipeTextList:
            if l.find('TAGS:') == 0:  # 3.0 incompatibility
                ts = l[5:]
                tlist = ts.split(',')
                if tlist:
                    for t in tlist:
                        st = t.strip()
                        newRec.taglist.append(st)
                        tagQuery = Tag.gql('where tag = :1',st)
                        if tagQuery.count() == 0:
                            newTag = Tag()
                            newTag.tag = st
                            newTag.put()

        newRec.put()
        newRec.index()
        me.recipes.append(newRec.key())
        me.put()
        memcache.delete_multi(["rlist_"+me.profile_url, "taglist_"+me.profile_url])  # invalidate rlist and tag caches
        self.redirect("/showrecipe/"+str(newRec.key()))
        

class ShowImport(BaseHandler):

    def get(self):
        path = os.path.join('templates','import.html')
        self.response.out.write(template.render(path,self.create_context()))


    
logging.getLogger().setLevel(logging.DEBUG)

#  for i in os.environ:
#      logging.debug(i + " : " + os.environ[i])

application = webapp2.WSGIApplication([('/', Home),
                                        ('/myindex', RecipeIndex),
                                        ('/help', Help),
                                        ('/settings', Settings),
                                        ('/savesettings', SettingsSave),
                                        ('/showrecipe/.*', RecipeDetail),
                                        ('/showtag/.*',TagList),
                                        ('/search', RecipeSearch),
                                        ('/setpublic',ChangePublic),
                                        ('/saveedit/.*',SaveEdit),
                                        ('/edit/.*',RecipeEdit),
                                        ('/public/.*',PublicIndex),
                                        ('/addremove',AddRemove),
                                        ('/beginimport', ShowImport),
                                        ('/save', RecipeSaver),
                                        ('/import', ImportHandler),                                        
                                        (r"/auth/login", LoginHandler),
                                        (r"/auth/logout", LogoutHandler),
                                        ],
                                       debug=True)
