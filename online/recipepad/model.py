from google.appengine.ext import db
from google.appengine.api import users
import search

class FBUser(db.Model):
    id = db.StringProperty(required=True)
    created = db.DateTimeProperty(auto_now_add=True)
    updated = db.DateTimeProperty(auto_now=True)
    name = db.StringProperty(required=True)
    profile_url = db.StringProperty(required=True)
    access_token = db.StringProperty(required=True)
    email = db.EmailProperty()
    recipes = db.ListProperty(db.Key)
    public_link = db.StringProperty()

class Recipe(search.Searchable,db.Model):
    submitted_by = db.ReferenceProperty(FBUser)
    title = db.StringProperty()
    recipeText = db.TextProperty()
    last_update = db.DateTimeProperty()
    cooks = db.ListProperty(db.Key)
    taglist = db.StringListProperty()
    public = db.BooleanProperty()

    INDEX_ONLY = ['recipeText']


class Tag(db.Model):
    tag = db.StringProperty()

# http://www.billkatz.com/2009/6/Simple-Full-Text-Search-for-App-Engine
#    - info on a better full text search extension for models
#

#class Cook_Recipe(db.Model):
#    user_id = db.UserProperty()
#    recipe = db.ReferenceProperty()
#
