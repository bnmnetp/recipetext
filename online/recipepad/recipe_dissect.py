#!/usr/bin/python

import sys
import re
import textwrap
import codecs
from titlecase import titlecase

# parse a recipe from stdin using some simple heuristics

# display with each line color coded so that its easy to correct and reorganize
# the parts that are incorrectly classified
# pale green for title
# pale blue for ingredients
# pale pink or yellow for directions
# categories?
# servings?
# other common aspects?

# Assume first line is the title


# the following property extension is from
# http://blog.devork.be/2008/04/xsetter-syntax-in-python-25.html
_property = property

class property(property):
    def __init__(self, fget, *args, **kwargs):
        self.__doc__ = fget.__doc__
        super(property, self).__init__(fget, *args, **kwargs)

    def setter(self, fset):
        cls_ns = sys._getframe(1).f_locals
        for k, v in cls_ns.iteritems():
            if v == self:
                propname = k
                break
        cls_ns[propname] = property(self.fget, fset,
                                    self.fdel, self.__doc__)
        return cls_ns[propname]

# Now find a line that contains the word ingredients or a synonym
# if no lines start with that word then find the first line that begins 
# with a measurement

class Recipe(object):
    """
    """

    def __init__(self, text):
        """
        Container object for all the parts of a recipe
        Arguments:
        - `title`:
        """
        self.rt = text.split('\n')
        self.__name = ""
        self.__ingredients = []
        self.__method = []
        self.__servings = ""
        self.__source = ""
        self.__tags = ""
        self.__notes = []
        self.__level = ""
        self.__time = ""
        self.__rating = ""

        self.blankLine = re.compile(r'^\s*$')

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self,value):
        while value[0] == "#":
            value = value[1:]
        value = value.strip()
        self.__name = value

    @property
    def ingredients(self):
        return self.__ingredients

    @property
    def method(self):
        return self.__method

    @property
    def servings(self):
        return self.__servings

    @servings.setter
    def servings(self,value):
        if value == None:
            raise ValueError('value is none')
        else:
            if value[:8] == 'SERVINGS':
                self.__servings = value[10:].strip()
            else:
                self.__servings = value

    @property
    def source(self):
        return self.__source

    @source.setter
    def source(self,value):
        self.__source = value

    @property
    def time(self):
        return self.__time

    @time.setter
    def time(self,value):
        self.__time = value
    
    @property
    def notes(self):
        return self.__notes

    @property
    def tags(self):
        return self.__tags

    @tags.setter
    def tags(self,value):
        self.__tags = value
    
    @property
    def level(self):
        return self.__level

    @level.setter
    def level(self,value):
        if value:
            self.__level = value
        else:
            value = ""
        
    @property
    def rating(self):
        return self.__rating

    def setRating(self,val):
        if val == None:
            print 'rating value is None'
            val = ""
        self.__rating = val

    @rating.setter
    def rating(self,value):
        self.setRating(value)

    def addIngredient(self,ingredient):
        ingredient = ingredient.strip()
        self.__ingredients.append(ingredient)

    def trimIngredients(self):
        while self.__ingredients and self.blankLine.match(self.__ingredients[0]):
            self.__ingredients = self.__ingredients[1:]
        while len(self.__ingredients) > 1 and self.__ingredients[-1].strip() == "":
            self.__ingredients = self.__ingredients[:-1]

    def addMethodText(self,mtext):
        self.__method.append(mtext)

    def trimMethod(self):
        while self.__method and self.blankLine.match(self.__method[0]):
            self.__method = self.__method[1:]
        while len(self.__method) > 1 and self.__method[-1].strip() == "":
            self.__method = self.__method[:-1]

    def trimNotes(self):
        while self.__notes and self.blankLine.match(self.__notes[0]):
            self.__notes = self.__notes[1:]
        while len(self.__notes) > 1 and self.__notes[-1].strip() == "":
            self.__notes = self.__notes[:-1]

    def addNote(self,nline):
        self.__notes.append(nline)

    def __str__(self):
        res = u""
        res += u"# " + self.name + u"\n\n"
        res += u"## INGREDIENTS:\n\n"
        for i in self.ingredients:
            res += u"- " + i + u"\n"
        res += u"\n\n"
        res += u"## METHOD:\n\n"
        for i in self.method:
            res += textwrap.fill(i) + u"\n"
        res += u"\n\n"
        res += u"SERVINGS: " + self.servings + u"\n"
        res += u"SOURCE: " + self.source + u"\n"
        res += u"DIFFICULTY: " + self.level + u"\n"
        res += u"PREP TIME: " + self.time + u"\n"
        res += u"RATING: " + self.rating + u"\n"
        res += u"TAGS: " + self.tags + u"\n"
        res += u"NOTES: \n\n"
        for i in self.notes:
            res += textwrap.fill(i) + u"\n"
        res += u"\n"
        return res

    def __unicode__(self):
        res = u""
        res += self.name + u"\n\n"
        res += u"INGREDIENTS:\n\n"
        for i in self.ingredients:
            if i[0] == '-' or i[0] == '*':
                res += i + u"\n"
            else:
                res += u'- ' + i + u'\n'
        res += u"\n\n"
        res += u"METHOD:\n\n"
        for i in self.method:
            res += textwrap.fill(i) + u"\n"
        res += u"\n\n"
        res += u"SERVINGS: " + self.servings + u"\n"
        res += u"SOURCE: " + self.source + u"\n"
        res += u"DIFFICULTY: " + self.level + u"\n"
        res += u"PREP TIME: " + self.time + u"\n"
        res += u"RATING: " + self.rating + u"\n"
        res += u"TAGS: " + self.tags + u"\n"
        res += u"NOTES: \n\n"
        for i in self.notes:
            res += textwrap.fill(i) + u"\n"
        res += u"\n"
        return res


class ImportedRecipe(Recipe):
    """
    """

    def __init__(self, text ):
        """
        """
        Recipe.__init__(self,text)
        self.all_heads = re.compile(r'(^method)|(^directions)|(^preparation)|(^instructions)|(^serving[s]?)|(^source)|(^level)|(^keyword[s]?)|(^category)|(^categories)|(^notes)|(^ingredients)|(^prep time)|(rating)|(tags)|(notes)|(^difficulty)|(^tip)',re.IGNORECASE)
        self.findTitle()
        self.findSource()
        self.findDifficulty()
        self.findServings()
        self.findPrepTime()
        self.findTags()
        self.findRating(r'((^rating[:]?)|(^rated[:]?))\s*([\d\w\s]+)',r'(^rating[:]?)\s*$',3,self.setRating)
        self.findIngredients()
        self.trimIngredients()
        self.findNotes()
        self.trimNotes()
        self.findDirections()
        self.trimMethod()

    def findTitle(self):
        """
        Given some text find the title
        Arguments:
        - `self`:
        - `rt`: recipe text from the clipboard
        """
        
        # eliminate leading blank lines
        while re.match(r'^\s*$',self.rt[0]):
            self.rt = self.rt[1:]

        if not re.match('^ingredients',self.rt[0],re.IGNORECASE):
            # probably the title
            self.name = titlecase(self.rt[0]).strip()
            self.rt = self.rt[1:]

    def findSource(self):
        start = 0
        src1 = re.compile(r'((^source[:]?)|(recipe courtesy[:]?)|(^(submitted )?by:)|(recipe by[:]?))\s+(.*)',re.IGNORECASE)
        src2 = re.compile(r'((^source[:]?)|(recipe courtesy[:]?))\s*$',re.IGNORECASE)        
        while start < len(self.rt):
            g = src1.match(self.rt[start])
            if g:
                self.source = self.rt[start][len(g.group(1)):].strip()
                self.rt = self.rt[:start] + self.rt[start+1:]
                return
            g = src2.match(self.rt[start])
            if g:
                self.source = self.rt[start+1]
                self.rt = self.rt[:start] + self.rt[start+2:]
                return
            start += 1

    def findTags(self):
        start = 0
        src1 = re.compile(r'((^tag[s]?[:]?)|(^keyword[s]?[:]?)|(category[:]?)|(categories[:]?))\s+(.*)$',re.IGNORECASE)
        src2 = re.compile(r'((^tag[s]?[:]?)|(^keyword[s]?[:]?)|(category[:]?)|(categories[:]?))\s*$',re.IGNORECASE)        
        while start < len(self.rt):
            g = src1.match(self.rt[start])
            if g:
                self.tags = g.group(6)
                self.rt = self.rt[:start] + self.rt[start+1:]
                return
            g = src2.match(self.rt[start])
            if g and start+1 < len(self.rt):
                self.tags = self.rt[start+1]
                self.rt = self.rt[:start] + self.rt[start+2:]
                return
            start += 1

    def findPrepTime(self):
        pt1 = re.compile(r'(((^prep)|(^preparation)|(^cook)|(^cooking)|(^total))?\s+time[:]?)\s*(\d+\s+\w+)',re.IGNORECASE)
        pt2 = re.compile(r'(\d+)\s+(\w+)\s+prep',re.IGNORECASE)
        start = 0
        while start < len(self.rt):
            g = pt1.match(self.rt[start])
            if g:
                self.time = self.rt[start][len(g.group(1)):].strip()
                self.rt = self.rt[:start] + self.rt[start+1:]
                return
            else:
                g = pt2.match(self.rt[start])
                if g:
                    self.time = g.group(1) + " " + g.group(2)
                    self.rt = self.rt[:start] + self.rt[start+1:]
                    return
            start += 1

    def findDifficulty(self):
        dif1 = re.compile(r'((^level[:]?)|(difficulty[:]?))\s+([\d\w]+)',re.IGNORECASE)
        dif2 = re.compile(r'(^level[:]?)|(difficulty[:]?)\s*$',re.IGNORECASE)
        start = 0
        while start < len(self.rt):
            g = dif1.match(self.rt[start])
            if g:
                self.level = g.group(4)
                self.rt = self.rt[:start] + self.rt[start+1:]
                return
            g = dif2.match(self.rt[start])
            if g:
                if self.all_heads.match(self.rt[start+1]):
                    return
                else:
                    self.level = self.rt[start+1]
                    self.rt = self.rt[:start] + self.rt[start+2:]
                return
            start += 1

    def findNotes(self):
        start = 0
        head = re.compile(r'((^note[s]?[:]?)|(^tip[:]?))',re.IGNORECASE)
        while start < len(self.rt):
            g = head.match(self.rt[start]) 
            if g:
                first_line = self.rt[start][len(g.group(1)):]
                if first_line:
                    self.addNote(first_line)
                begin_notes = start
                start += 1
                while start < len(self.rt) and not self.all_heads.match(self.rt[start]):
                    self.addNote(self.rt[start])
                    start += 1
                self.rt = self.rt[:begin_notes] + self.rt[start:]
                return
            start += 1

    def findRating(self,slre,tlre,vgroup,setter):
        start = 0
        one_line = re.compile(slre,re.IGNORECASE)
        two_line = re.compile(tlre,re.IGNORECASE)
        while start < len(self.rt):
            g = one_line.match(self.rt[start])
            if g:
                setter(self.rt[start][len(g.group(1)):].strip())
                self.rt = self.rt[:start] + self.rt[start+1:]
                return
            g = two_line.match(self.rt[start])
            if g:
                setter(self.rt[start+1])
                self.rt = self.rt[:start] + self.rt[start+2:]
                return
            start += 1
                
    def findIngredients(self):
        # find ingredients or the first line starting with a number
        start = 0
        while start < len(self.rt) and not re.match(r'^ingredients',self.rt[start],re.IGNORECASE):
            start += 1
        if start == len(self.rt):
            start = 0
            while not re.match(r'[0-9]+.*',self.rt[start],re.IGNORECASE):
                start += 1
        if start < len(self.rt):
            if re.match(r'^ingredients',self.rt[start],re.IGNORECASE):
                start += 1
            save_start = start
            done = False
            oneBlank = False
            while not done:
                if self.all_heads.match(self.rt[start]):
                    done = True
                    break
                elif re.match(r'(^\s*$)',self.rt[start],re.IGNORECASE):
                    if oneBlank:
                        done = True
                    else:
                        oneBlank = True
                else:
                    self.addIngredient(self.rt[start])
                    oneBlank = False
                start += 1
                if start >= len(self.rt):
                    done = True
            self.rt = self.rt[:save_start] + self.rt[start:]
        else:
            print 'did not find any ingredients'


    def findDirections(self):
        start = 0
        while start < len(self.rt) and not re.match(r'(method)|(directions)|(preparation)|(instructions)',self.rt[start],re.IGNORECASE):
            start += 1
        if start < len(self.rt):
            if re.match(r'(method)|(directions)|(preparation)|(instructions)',self.rt[start],re.IGNORECASE):
                start += 1
            while start < len(self.rt) and not self.all_heads.match(self.rt[start]):
                self.addMethodText(self.rt[start])
                start += 1
        else:
            for line in self.rt:
                self.addMethodText(line)

 
    def findServings(self):
        sre1 = re.compile(r'(\d+)\s+(serving[s]*)',re.IGNORECASE)
        sre2 = re.compile(r'((serving[s]*[:]*)|(serves[:]?)|(makes[:]?)|(yield[s]*[:]*)|(serving size[:]?))\s+(\d+)',re.IGNORECASE)
        sre3 = re.compile(r'(serves[:]*)\s*$',re.IGNORECASE)
        start = 0
        done = False
        while not done and start < len(self.rt):
            m1 = sre1.match(self.rt[start])
            m2 = sre2.match(self.rt[start])
            m3 = sre3.match(self.rt[start])            
            if m1 or m2:
                done = True
                if m1:
                    self.servings = m1.group(1)
                else:
                    self.servings = self.rt[start][len(m2.group(1)):].strip()
                self.rt = self.rt[:start] + self.rt[start+1:]
            elif m3:
                g = re.match(r'(\d+).*',self.rt[start+1],re.IGNORECASE)
                if g:
                    self.servings = g.group(1)
                    self.rt = self.rt[:start] + self.rt[start+2:]
            start += 1

#########################################################################
#
#   Parsed Recipe Class
#
#########################################################################
class ParsedRecipe(Recipe):
    """ This is a class for reading and working with recipes that have already been  stored in my format.
    """

    def __init__(self, text):
        """
        
        Arguments:
        - `text`:
        """
        Recipe.__init__(self,text)

        self.__sections = {'INGREDIENTS':-1,
                           'METHOD':-1,
                           'SERVINGS':-1,
                           'SOURCE':-1,
                           'DIFFICULTY':-1,
                           'PREP TIME':-1,
                           'RATING':-1,
                           'TAGS':-1,
                           'NOTES':-1
                           }

        self.parseIt()


    def parseIt(self):
        """
        Load and parse a recipe stored in my format
        Arguments:
        - `self`:
        """

        for sect in self.__sections:
            self.__sections[sect] = self.findLine(sect)

        self.name = self.rt[0].strip()

        for i in range(self.__sections['INGREDIENTS']+2,
                       self.__sections['METHOD']-1):
            self.addIngredient(self.rt[i])
            self.trimIngredients()
            
        for i in range(self.__sections['METHOD']+2,
                       self.methodEnd()):
            self.addMethodText(self.rt[i])
        self.trimMethod()
        
        if self.__sections['SERVINGS'] > -1:
            self.servings = self.rt[self.__sections['SERVINGS']][10:].strip()
        if self.__sections['SOURCE'] > -1:
            self.source = self.rt[self.__sections['SOURCE']][8:].strip()
        if self.__sections['DIFFICULTY'] > -1:
            self.level = self.rt[self.__sections['DIFFICULTY']][12:].strip()
        if self.__sections['PREP TIME'] > -1:
            self.time = self.rt[self.__sections['PREP TIME']][11:].strip()
        if self.__sections['RATING'] > -1:
            self.rating = self.rt[self.__sections['RATING']][8:].strip()
        if self.__sections['TAGS'] > -1:
            self.tags = self.rt[self.__sections['TAGS']][6:].strip()
        if self.__sections['NOTES'] > -1:
            for i in range(self.__sections['NOTES']+2,
                           len(self.rt)):
                self.addNote(self.rt[i])
            self.trimNotes()


    def findLine(self,text):
        lc = 0
        for line in self.rt:
            if line.find(text) > -1:
                return lc
            lc += 1
        return -1

    def methodEnd(self):
        if self.__sections['SERVINGS'] > -1:
            return self.__sections['SERVINGS']
        elif self.__sections['SOURCE'] > -1:
            return self.__sections['SOURCE']
        elif self.__sections['DIFFICULTY'] > -1:
            return self.__sections['DIFFICULTY']
        elif self.__sections['PREP TIME'] > -1:
            return self.__sections['PREP TIME']
        elif self.__sections['RATING'] > -1:
            return self.__sections['RATING']
        elif self.__sections['NOTES'] > -1:
            return self.__sections['NOTES']
        else:
            return len(self.rt)

    def toHTML(self,fullpage=True):
        """
        Convert this recipe object into its html representation.
        name -- h1 and title
        ingredients - h2 -- div around all ingredients
        method - h2 -- div around method  -- replace blank lines with </p><p>
        -- div around all of the following
        servings - h3 around SERVINGS - span around #
        source -
        difficulty
        prep_time
        rating
        tags
        --- 
        notes h3 -- div around all notes
        Arguments:
        - `self`:
        """
        if fullpage:
            hstring =  u'''
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" href="recstyle.css" type="text/css" media="screen" />
<title>%s</title>
</head>
<body>
<div id="heading">
<h1 class="title">%s</h1>
</div>
<div id="leftcolumn">
<div id="ingredients">
<h2>Ingredients</h2>
<ul>
'''    %  (self.name, self.name)
        else:
            hstring = u'''
<div id="leftcolumn">
<div id="ingredients">
<h2>Ingredients</h2>
<ul>
'''
        # ingredients
        hstring += htmlIngredients()

        # method
        hstring += htmlMethod()
        
        #notes
        hstring += htmlNotes()

        # other
        hstring += htmlInfo()

        if fullpage:
            hstring += u'</body>\n'
            hstring += u'</html>\n'

        return hstring

    def htmlIngredients(self):
        hstring = u'<h2>Ingredients</h2>\n'
        hstring += u'<ul class="inglist">'
        for i in self.ingredients:
            if i[0] == "":
                continue
            elif i[0] == "-":
                hstring += u"<li>%s</li>" % i[1:]
            elif i[0] == "*":
                hstring += u'</ul><h4>%s</h4><ul class="inglist">' % i[1:]
            else:
                hstring += u"<li>%s</li>" % i
        hstring += u'</ul>'
        return hstring

    def htmlMethod(self):
        hstring = u'<div id="method"><h2>Method</h2>\n'
        for i in range(len(self.method)):
            if self.method[i].strip() == "":
                self.method[i] = u"</p><p>\n"
        hstring += u"<p>"
        hstring += u" ".join(self.method)
        hstring += u"</p>\n"
        hstring += u"</div>\n"
        return hstring

    def htmlNotes(self):
        hstring = u'<div id="notes"><h3>Notes</h3>\n'
        for i in range(len(self.notes)):
            if self.notes[i].strip() == "":
                self.notes[i] = "</p><p>\n"
        hstring += u"<p>\n"
        hstring += u" ".join(self.notes)
        hstring += u"</p>\n"
        hstring += u"</div>\n"
        return hstring

    def htmlInfo(self):
        hstring = u'<div id="info">\n'
        # servings
        hstring += u'<table>\n'
        hstring += u'<tr><td><h4>Servings:</h4></td>'
        hstring += u'<td class="servings">%s</td></tr>' % self.servings
        # source
        hstring += u'<tr><td><h4>Source:</h4></td>'
        hstring += u'<td class="source">%s</td></tr>' % self.source
        # difficulty
        hstring += u'<tr><td><h4>Difficulty:</h4></td>'
        hstring += u'<tr><td><p class="difficulty">%s</td></tr>' % self.level
        # prep time
        hstring += u'<tr><td><h4>Time:</h4></td>'
        hstring += u'<td class="time">%s</td></tr>' % self.time
        # rating
        hstring += u'<tr><td><h4>Rating:</h4></td>'
        hstring += u'<td class="rating">%s</td></tr>' % self.rating
        # tags
        hstring += u'<tr><td><h4>Tags:</h4></td>'
        hstring += u'<td class="tags">%s</td></tr>' % self.tags

        hstring += u'</table></div>\n'
        return hstring
    
if __name__ == '__main__':

#    rf = codecs.open('/Users/bmiller/Recipes/import/Chocolate Chip Butterscotch Pie.txt','r','utf-8')
#    rf = codecs.open('/Users/bmiller/tmp/Users/bmiller/Dropbox/Millers/Recipes/Fish Tacos.txt','r','utf-8')
#    rf = codecs.open('/Users/bmiller/Dropbox/Projects/Recipe/test/chow_lambic.txt','r','utf-8')
#    rf = codecs.open('/Users/bmiller/Dropbox/Projects/Recipe/test/steakwithpotatorisotto.txt','r','utf-8')    
    rf = codecs.open(sys.argv[1],'r','utf-8')
    recipeText = rf.read()
    myRecipe = ImportedRecipe(recipeText)
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
    sys.stdout.write(unicode(myRecipe))
#    of = codecs.open('../expect/'+sys.argv[1],'w','utf-8')
#    of.write(unicode(myRecipe))




    # rf = codecs.open('../test/thai_cooksill.txt','r','utf-8')
    # recipeText = rf.read()
    # myRecipe = ImportedRecipe(recipeText)
    # print unicode(myRecipe)

    # rf = codecs.open('../test/epi.txt','r','utf-8')
    # recipeText = rf.read()
    # myRecipe = ImportedRecipe(recipeText)
    # print unicode(myRecipe)

    # rf = codecs.open('../test/ChickenEnchiladas.txt','r','utf-8')
    # recipeText = rf.read()
    # myRecipe = ImportedRecipe(recipeText)
    # print unicode(myRecipe)

    # rf = codecs.open('../test/scanned.txt','r','utf-8')
    # recipeText = rf.read()
    # myRecipe = ImportedRecipe(recipeText)
    # print myRecipe

    # rf = codecs.open('../test/allrecipetoffee.txt','r','utf-8')
    # recipeText = rf.read()
    # myRecipe = ImportedRecipe(recipeText)
    # print unicode(myRecipe)

    # rf = codecs.open('../test/allrecprint.txt','r','utf-8')
    # recipeText = rf.read()
    # myRecipe = ImportedRecipe(recipeText)
    # print unicode(myRecipe)

    # rf = open('../test/splendidtable.txt')
    # recipeText = rf.read()
    # myRecipe = Recipe(recipeText)
    # print myRecipe
