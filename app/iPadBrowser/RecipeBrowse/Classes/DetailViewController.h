//
//  DetailViewController.h
//  RecipeBrowse
//
//  Created by Bradley Miller on 7/14/10.
//  Copyright Luther College 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MyUserPreferences;
@class RecipeList;

@interface DetailViewController : UIViewController <UIPopoverControllerDelegate, UISplitViewControllerDelegate> {
    
    UIPopoverController *popoverController;
    UIToolbar *toolbar;
    
    id detailItem;
    UILabel *detailDescriptionLabel;
	IBOutlet UIWebView *recipeDetail;
	IBOutlet MyUserPreferences *preferenceView;
    IBOutlet UIActivityIndicatorView *syncActInd;
}

-(IBAction) showPrefs;
-(IBAction) syncRecipes;

@property (nonatomic, retain) IBOutlet UIToolbar *toolbar;
@property (nonatomic, retain) UIPopoverController *popoverController;
@property (nonatomic, retain) id detailItem;
@property (nonatomic, retain) IBOutlet UILabel *detailDescriptionLabel;

@property (nonatomic, retain) IBOutlet MyUserPreferences *preferenceView;
@property (nonatomic, retain) IBOutlet UIWebView *recipeDetail;

@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *syncActInd;

@end
