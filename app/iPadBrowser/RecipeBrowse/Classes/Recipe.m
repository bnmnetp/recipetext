//
//  Recipe.m
//  RecipeParse
//
//  Created by Bradley Miller on 8/31/10.
//  Copyright 2010 Luther College. All rights reserved.
//

#import "Recipe.h"


@implementation Recipe

@synthesize title,ingredients,method,servings,source,level,time,rating,tags,notes;

static NSString *getStrippedValue(NSArray *recipeLines, NSDictionary *sections, NSString *key) {
	int index = [key length] + 2;
	NSString *res = [[[recipeLines objectAtIndex:[[sections objectForKey:key] intValue]] substringFromIndex:index] stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
	return res;
}

-(id) initWithString:(NSString *)recipeText {
	
	NSArray *recipeLines = [recipeText componentsSeparatedByString:@"\n"];
	NSMutableDictionary *sections = [NSMutableDictionary dictionaryWithObjectsAndKeys:
									 [NSNumber numberWithInt:-1],@"INGREDIENTS",
									 [NSNumber numberWithInt:-1],@"METHOD",
									 [NSNumber numberWithInt:-1],@"SERVINGS",
									 [NSNumber numberWithInt:-1],@"SOURCE",
									 [NSNumber numberWithInt:-1],@"DIFFICULTY",
									 [NSNumber numberWithInt:-1],@"PREP TIME",
									 [NSNumber numberWithInt:-1],@"RATING",
									 [NSNumber numberWithInt:-1],@"TAGS",							  
									 [NSNumber numberWithInt:-1],@"NOTES",							  							  
									 nil];
	title = [recipeLines objectAtIndex: 0];
	
	for	(NSString *key in [sections allKeys]) {
		int i = 0;
		// Find starting point for each
		for (NSString *line in recipeLines) {
			if ([line hasPrefix:key]) {
				[sections setObject:[NSNumber numberWithInt:i] forKey:key];
			}
			i++;
		}
		
	}
	NSRange aRange;
	aRange.location = [[sections objectForKey:@"INGREDIENTS"] intValue] + 2;
	aRange.length = [[sections objectForKey:@"METHOD"] intValue] - aRange.location - 1;
	ingredients = [recipeLines subarrayWithRange:aRange];
	
	aRange.location = [[sections objectForKey:@"METHOD"] intValue] + 2;
	aRange.length = [[sections objectForKey:@"SERVINGS"] intValue] - aRange.location - 1;
	method = [recipeLines subarrayWithRange:aRange];
	
	aRange.location = [[sections objectForKey:@"NOTES"] intValue] + 2;
	aRange.length = [recipeLines count] - aRange.location - 2;
	notes = [recipeLines subarrayWithRange:aRange];
	NSLog(@"Notes are %@",notes);
	
	if ([[sections objectForKey:@"SOURCE"] intValue] > -1) {
		//servings = [[[recipeLines objectAtIndex:[servings objectForKey:@"SERVINGS"]] substringFromIndex:10] stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
		source = getStrippedValue(recipeLines, sections, @"SOURCE");
	}
	
	if ([[sections objectForKey:@"SERVINGS"]intValue] > -1) {
		servings = getStrippedValue(recipeLines, sections, @"SERVINGS");
	}
	
	if ([[sections objectForKey:@"DIFFICULTY"]intValue] > -1) {
		level = getStrippedValue(recipeLines, sections, @"DIFFICULTY");
	}
	
	if ([[sections objectForKey:@"PREP TIME"]intValue] > -1) {
		time = getStrippedValue(recipeLines, sections, @"PREP TIME");
	}
	
	if ([[sections objectForKey:@"RATING"]intValue] > -1) {
		rating = getStrippedValue(recipeLines, sections, @"RATING");
	}
	
	if ([[sections objectForKey:@"TAGS"]intValue] > -1) {
		NSString *tagStr = getStrippedValue(recipeLines, sections, @"TAGS");
		tags = [tagStr componentsSeparatedByString:@","];
	}
	
	return self;
}

-(NSString *)htmlIngredients {
	NSString *hstring = [NSString stringWithFormat:@"<h2>Ingredients</h2>\n"];
	hstring = [hstring stringByAppendingString:@"<ul class=\"inglist\">"];
	for	(NSString *i in ingredients) {
		// test for empty string
		// test for beginning -
		if ([[i stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
			continue;
		}
		if ([i hasPrefix:@"-"]) {
			hstring = [hstring stringByAppendingFormat:@"<li>%@</li>\n",[i substringFromIndex:1]];
		}
		if ([i hasPrefix:@"*"]) {
			hstring = [hstring stringByAppendingFormat:@"</ul>\n<h4>%@</h4>\n<ul class=\"inglist\">\n",[i substringFromIndex:1]];
		}
		
	}
	hstring = [hstring stringByAppendingString:@"</ul>"];
	return hstring;
}

-(NSString *)htmlMethod {
	NSString *hstring = [NSString stringWithString:@"<div id=\"method\" style=\"width:40em;\"><h2>Method</h2>\n<p>"];
	for (NSString *i in method) {
		if ([[i stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
			hstring = [hstring stringByAppendingString:@"</p><p>\n"];
		} else {
			hstring = [hstring stringByAppendingFormat:@" %@ ",i];
		}
		
	}
	hstring = [hstring stringByAppendingString:@"</p>\n</div>\n"];
	return hstring;
}


-(NSString *)htmlNotes {
	NSString *hstring = [NSString stringWithString:@"<div id=\"notes\"><h3>Notes</h3>\n"];
	hstring = [hstring stringByAppendingString:@"<p>"];
	for (NSString *i in notes) {
		if ([[i stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0) {
			hstring = [hstring stringByAppendingString:@"</p><p>"];
		} else {
			hstring = [hstring stringByAppendingFormat:@" %@ ",i];
		}
	}
	hstring = [hstring stringByAppendingString:@"</p>\n</div>\n"];
	return hstring;
}


-(NSString *)htmlInfo {
	NSString *hstring = [NSString stringWithString:@"<div id=\"info\">\n<table>"];
	hstring = [hstring stringByAppendingString:@"<tr><td width=\"100px;\" style=\"font-weight: bold;\">Servings:</td>\n"];
	hstring = [hstring stringByAppendingFormat:@"<td class=\"servings\">%@</td></tr>",servings];
	
	hstring = [hstring stringByAppendingString:@"<tr><td style=\"font-weight: bold;\">Source</td>\n"];
	hstring = [hstring stringByAppendingFormat:@"<td class=\"servings\">%@</td></tr>",source];
	
	hstring = [hstring stringByAppendingString:@"<tr><td style=\"font-weight: bold;\">Difficulty:</td>\n"];
	hstring = [hstring stringByAppendingFormat:@"<td class=\"servings\">%@</td></tr>",level];
	
	hstring = [hstring stringByAppendingString:@"<tr><td style=\"font-weight: bold;\">Prep Time:</td>\n"];
	hstring = [hstring stringByAppendingFormat:@"<td class=\"servings\">%@</td></tr>",time];
	
	hstring = [hstring stringByAppendingString:@"<tr><td style=\"font-weight: bold;\">Rating:</td>\n"];
	hstring = [hstring stringByAppendingFormat:@"<td class=\"servings\">%@</td></tr>",rating];
	
	hstring = [hstring stringByAppendingString:@"<tr><td style=\"font-weight: bold;\">Tags:</td>\n"];
	hstring = [hstring stringByAppendingFormat:@"<td class=\"servings\">%@</td></tr>",[tags componentsJoinedByString:@", "]];
	hstring = [hstring stringByAppendingFormat:@"</table></div>"];
	
	return hstring;
}

-(NSString *)htmlStringFromRecipe {
    NSString *cssInclude = [NSString stringWithString:@"<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />"];
    cssInclude = [cssInclude stringByReplacingOccurrencesOfString:@"style.css" 
                                                       withString:[NSString stringWithFormat:@"style.css?time=%@", 
                                                                   [[NSDate date] description]]];
	NSString *hstring = [NSString stringWithFormat:@"<html><head><title>%@</title>%@</head><body>\n",title, cssInclude];
	hstring = [hstring stringByAppendingFormat:@"<h1>%@</h1>\n",title];
	hstring = [hstring stringByAppendingString:[self htmlIngredients]];
	hstring = [hstring stringByAppendingString:[self htmlMethod]];
	hstring = [hstring stringByAppendingString:[self htmlNotes]];
	hstring = [hstring stringByAppendingString:[self htmlInfo]];
	hstring = [hstring stringByAppendingString:@"</body></html>"];
	
	return hstring;
	
}

@end
