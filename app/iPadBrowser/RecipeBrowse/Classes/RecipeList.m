//
//  RecipeList.m
//  RecipeBrowse
//
//  Created by Brad Miller on 7/29/10.
//  Copyright (c) 2010 Luther College. All rights reserved.
//

#import "RecipeList.h"
#import "GTMNSString+URLArguments.h"
#import "FMDatabase.h"
#import "Recipe.h"

@implementation RecipeList

static RecipeList *sharedList = nil;
static UIActivityIndicatorView *pv = nil;

NSString *kAppId = @"71806252102";
NSString *kApiHost = @"recipepad.appspot.com";
//NSString *kApiHost = @"10.0.1.3:8083";  

-(NSUInteger) count {
    return [recipeList  count];
}

-(NSString *) titleAtIndex:(NSUInteger)index {
    // [cell.textLabel.text substringToIndex:[cell.textLabel.text length]-4];
    return [recipeList objectAtIndex:index];
}

-(NSString *) recipeAtIndex:(NSUInteger)index {
	NSString *dbFilePath = [NSString stringWithFormat:@"%@/%@",NSHomeDirectory(),@"Documents/recipedb"];
	FMDatabase *db = [FMDatabase databaseWithPath:dbFilePath];
	[db open];
	
	FMResultSet *rs = [db executeQuery:@"select recipe_text from recipe where title = ?",
					   [recipeList objectAtIndex:index]];
	
	[rs next];
	NSString *recipe = [rs stringForColumnIndex:0];
	[rs close];
	[db close];
    return recipe;
}

-(NSString *) recipeHTMLStringAtIndex:(NSUInteger) index {
	NSString *strRecipe = [self recipeAtIndex:index];
	Recipe *theRecipe = [[Recipe alloc] initWithString:strRecipe];
	return [theRecipe htmlStringFromRecipe];
}

- (NSString *) recipeHTMLStringAtIndex:(NSUInteger) index saveAsLastViewed: (BOOL) savep {
    NSString *recString = [self recipeHTMLStringAtIndex:index];
    if (savep) {
        NSUserDefaults *defaultObject = [NSUserDefaults standardUserDefaults];
        [defaultObject setObject:recString forKey:@"last_recipe"];
    }

    return recString;
}

/* Search */

-(BOOL) recipesMatchingQuery:(NSString *) searchFor {
	NSString *dbFilePath = [NSString stringWithFormat:@"%@/%@",NSHomeDirectory(),@"Documents/recipedb"];
	FMDatabase *db = [FMDatabase databaseWithPath:dbFilePath];
	[db open];

    NSString *qString = [NSString stringWithFormat:@"select title from recipe where "];
	NSArray *searchWordList = [searchFor componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	int i;
	for (i = 0; i < [searchWordList count]-1; i++) {
		qString = [qString stringByAppendingFormat:@"recipe_text like '%%%@%%' and ",[searchWordList objectAtIndex:i]];
	}
	qString = [qString stringByAppendingFormat:@"recipe_text like '%%%@%%' order by title",[searchWordList objectAtIndex:i]];
    NSLog(@"query string = %@",qString);
	
	FMResultSet *rs = [db executeQuery:qString];
	NSMutableArray *searchResults = [[NSMutableArray alloc] init];
	while ([rs next]) {
		[searchResults addObject:[rs stringForColumnIndex:0]];
	}

	[recipeList release];
    recipeList = searchResults; 
    return YES;

}

#pragma mark -
#pragma mark Lifecycle 


-(void) resetRecipeList {
	NSMutableArray *tempArray = [[NSMutableArray alloc] init];
	NSString *dbFilePath = [NSString stringWithFormat:@"%@/%@",NSHomeDirectory(),@"Documents/recipedb"];
	FMDatabase *db = [FMDatabase databaseWithPath:dbFilePath];
	[db open];
	FMResultSet *rs = [db executeQuery:@"select title from recipe order by title"];
	if ([db hadError]) {
        NSLog(@"Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
    }
	while ([rs next]) {
		[tempArray addObject:[rs stringForColumnIndex:0]];
	}
	[rs close];
	[db close];
	
	[recipeList release];
    recipeList = [[NSArray alloc] initWithArray:tempArray copyItems:YES];
    NSLog(@"recipe list count = %d",[recipeList count]);
}



/*  awakeFromNib
    called when the nib file that 'declares' this class is loaded, in this case RecipeViewController.nib
    When the nib is loaded is a good time to initialize the list.
*/
-(void)awakeFromNib {
	NSLog(@"Waking up from NIB!!!");
	if (sharedList == nil) {
		sharedList = self;
		NSLog(@"initializing shared object");
	}
	NSFileManager *dbFile = [[NSFileManager alloc] init];
	NSString *dbFilePath = [NSString stringWithFormat:@"%@/%@",NSHomeDirectory(),@"Documents/recipedb"];
	
	if (! [dbFile fileExistsAtPath:dbFilePath]) {
		NSString *blankDB = [NSString stringWithFormat:@"%@/%@",
							 [[NSBundle mainBundle] resourcePath],
							 @"recipedb"];
		[dbFile copyItemAtPath:blankDB toPath:dbFilePath error:NULL];
		NSLog(@"Created New DB in Documents");
	}
	
    [self resetRecipeList];

	[self syncRecipes];
	
}

-(void) dealloc {
	
	[recipeList release];
	[super dealloc];
}

+ (RecipeList *)getSharedObject {
	if (sharedList != nil) {
		return sharedList;
	}
	return nil;
}


#pragma mark -
#pragma mark Synchronization


-(void) syncRecipes {

    UIActivityIndicatorView *parentView = (UIActivityIndicatorView *)[[[UIApplication sharedApplication] keyWindow] viewWithTag: 777];
    NSLog(@"object for tag 777 is: %@", parentView);
    [parentView startAnimating];
	NSUserDefaults *myDefaults = [NSUserDefaults standardUserDefaults];
    UIActivityIndicatorView *myActInd = [[UIActivityIndicatorView alloc] init];
    [myActInd startAnimating];
	// if we have an access token or more importantly for phase one the uid stored in user prefs then just grab it.
	fbuid = [myDefaults stringForKey:@"FacebookUID"];
	[fbuid retain];
    pv = parentView; 

	if (fbuid) {
		[self sync];
	} else if (parentView) {
		NSArray *myPermissions =  [[NSArray arrayWithObjects: 
								  @"read_stream", @"offline_access",nil] retain];
		facebook = [[Facebook alloc] init];
		[facebook retain];
		[facebook authorize:kAppId permissions:myPermissions delegate:self];
	}
    

}

-(void) sync {
	NSString *dbFilePath = [NSString stringWithFormat:@"%@/%@",NSHomeDirectory(),@"Documents/recipedb"];
	NSLog(@"success");
	
	FMDatabase* db = [FMDatabase databaseWithPath:dbFilePath];
    if (![db open]) {
        NSLog(@"Could not open db.");
        return;
    }
	
    NSDate *lastSync = [NSDate dateWithTimeIntervalSince1970:86400];
    NSUserDefaults *myDefaults = [NSUserDefaults standardUserDefaults];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSString *syncDateString = [myDefaults stringForKey:@"lastsync"];
    if (syncDateString == nil) {
        lastSync = [NSDate dateWithTimeIntervalSince1970:86400];
    } else {
		lastSync = [df dateFromString:syncDateString];
	}
	
    syncDateString = [df stringFromDate:lastSync];
    syncDateString = [syncDateString stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
	
    NSLog(@"Escaped string looks like: %@",syncDateString);
	NSString *syncURL = [NSString stringWithFormat:@"http://%@/api/%@/sync?lastsync=%@",kApiHost,fbuid,syncDateString];
	NSArray *uArray = [NSArray arrayWithContentsOfURL:[NSURL URLWithString:syncURL]];	
	NSLog(@"Sync Query returned %d items", [uArray count]);
	[df setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSSSSS"];
	for (int i = 0; i < [uArray count]; i+=3) {
		FMResultSet *rs = [db executeQuery:@"select * from recipe where google_key = ?", [uArray objectAtIndex:i]];
		[rs next];
		if (! [rs hasAnotherRow]) {
			NSLog(@"Did not find a db entry for %@",[uArray objectAtIndex:i]);	
     		NSLog(@"last modified = %@",[df stringFromDate:[NSDate date]] );
     		NSLog(@"last modified as date = %@",[df dateFromString:[uArray objectAtIndex:i+2]] );			
			NSString *recipeText = [NSString stringWithContentsOfURL:[NSURL URLWithString:
																	  [NSString stringWithFormat:
																	   @"http://%@/api/%@/getrecipe/%@",
																	   kApiHost,
																	   fbuid,
																	   [uArray objectAtIndex:i]]] encoding:NSUTF8StringEncoding error:NULL];
			[db executeUpdate:@"insert into recipe (title, google_key, recipe_text, last_update) values(?, ?, ?,?)",
			 [uArray objectAtIndex:i+1],
			 [uArray objectAtIndex:i],
			 recipeText,
			 [df dateFromString:[uArray objectAtIndex:i+2] ] ];
		} else {
			NSLog(@"comparing db date %@ with google date %@",[rs dateForColumn:@"last_update"],[df dateFromString:[uArray objectAtIndex:i+2]]);
			if ([rs dateForColumn:@"last_update"] < [df dateFromString:[uArray objectAtIndex:i+2]]) {
				NSLog(@"Updating a recipe for %@",[uArray objectAtIndex:i+1]);
				[db executeUpdate:@"update recipe set title = ?, recipe_text = ?, last_update = ? where google_key = ?", 
				 [uArray objectAtIndex:i+1],
				 [NSString stringWithContentsOfURL:[NSURL URLWithString:
													[NSString stringWithFormat:
													 @"http://%@/api/%@/getrecipe/%@",
													 kApiHost,
													 fbuid,
													 [uArray objectAtIndex:i]]] encoding:NSUTF8StringEncoding error:NULL],
				 [df dateFromString:[uArray objectAtIndex:i+2] ],
				 [uArray objectAtIndex:i]];
			}
		}
		[rs close];
	}
	[df setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
	[myDefaults	setObject:[df stringFromDate:[NSDate date]] forKey:@"lastsync"];
	[db close];
	[self resetRecipeList];
    [pv stopAnimating];
	NSLog(@"success done");
	
}



#pragma mark -
#pragma mark Facebook Session Delegate Protocol


/**
 * Called when the dialog successful log in the user
 */
- (void)fbDidLogin {
	[facebook requestWithGraphPath:@"me" andDelegate:self];
	
}

/**
 * Called when the user dismiss the dialog without login
 */
- (void)fbDidNotLogin:(BOOL)cancelled {
	
}

/**
 * Called when the user is logged out
 */
- (void)fbDidLogout {
	
}


#pragma mark -
#pragma mark Facebook Request Delegate Protocol


/**
 * Called when a request returns and its response has been parsed into an object.
 *
 * The resulting object may be a dictionary, an array, a string, or a number, depending
 * on thee format of the API response.
 */
- (void)request:(FBRequest*)request didLoad:(id)result {
	NSMutableDictionary *fbRes;
	fbRes = (NSMutableDictionary *)result;
	fbuid = [fbRes objectForKey:@"id"];
	if (fbuid) {
		NSUserDefaults *myUserDefaults = [NSUserDefaults	standardUserDefaults];
		[myUserDefaults setObject:fbuid forKey:@"FacebookUID"];
		[self sync];
	}
}


@end
