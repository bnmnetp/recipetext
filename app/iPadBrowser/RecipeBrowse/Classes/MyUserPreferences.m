    //
//  MyUserPreferences.m
//  RecipeBrowse
//
//  Created by Bradley Miller on 8/19/10.
//  Copyright 2010 Luther College. All rights reserved.
//

#import "MyUserPreferences.h"


@implementation MyUserPreferences

@synthesize userField, passField;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	NSLog(@"Nib for preferences loaded");
	// prepopulate username field here...
	NSUserDefaults *defaultObject = [NSUserDefaults standardUserDefaults];
    NSString *user = [defaultObject stringForKey:@"username"];
	NSString *pass = [defaultObject stringForKey:@"password"];
	
	if (! user) {
		user = @"user@gmail.com";
		[defaultObject setObject:user forKey:@"username"];
	}
	
	if (! pass) {
		pass = @"mypass";
		[defaultObject setObject:pass forKey:@"password"];
	}

	userField.text = user;
	passField.text = pass;
}

- (IBAction)saveUserPrefs {
	NSLog(@"user = %@",userField.text);
	NSLog(@"pw = %@",passField.text);
	NSUserDefaults *defaultObject = [NSUserDefaults standardUserDefaults];
	if (userField.text) {
		[defaultObject setObject:userField.text forKey:@"username"];
	}

	if (passField.text) {
		[defaultObject setObject:passField.text forKey:@"password"];
	}
	[defaultObject synchronize];
	[self dismissModalViewControllerAnimated:YES];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
