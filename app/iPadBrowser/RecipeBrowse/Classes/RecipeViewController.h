//
//  RecipeViewController.h
//  RecipeBrowse
//
//  Created by Brad Miller on 7/28/10.
//  Copyright (c) 2010 Luther College. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailViewController.h"
#import "RecipeList.h"

@interface RecipeViewController : UITableViewController {
    DetailViewController *detailViewController;
	RecipeList *recipes;  
    UISearchBar *searchBar;
}

@property (nonatomic, retain) IBOutlet DetailViewController *detailViewController;
@property (nonatomic, retain) IBOutlet RecipeList *recipes;
@property (nonatomic, retain) IBOutlet UISearchBar *searchBar;
@end
