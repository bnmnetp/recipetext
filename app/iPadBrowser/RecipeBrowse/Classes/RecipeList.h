//
//  RecipeList.h
//  RecipeBrowse
//
//  Created by Brad Miller on 7/29/10.
//  Copyright (c) 2010 Luther College. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GoogleAppEngineAuth.h"
#import "DetailViewController.h"
#import	"FBConnect.h"

@interface RecipeList : NSObject <FBSessionDelegate,FBRequestDelegate> {
    NSArray *recipeList;
	Facebook *facebook;
	NSString *fbuid;
}

- (NSUInteger) count;
- (NSString *) titleAtIndex:(NSUInteger) index;
- (NSString *) recipeAtIndex:(NSUInteger) index;
- (NSString *) recipeHTMLStringAtIndex:(NSUInteger) index;
- (NSString *) recipeHTMLStringAtIndex:(NSUInteger) index saveAsLastViewed: (BOOL) savep;
- (BOOL) recipesMatchingQuery:(NSString *) searchFor;
- (void) resetRecipeList;
- (void) syncRecipes;
- (void) sync;

+ (RecipeList *)getSharedObject;
@end
