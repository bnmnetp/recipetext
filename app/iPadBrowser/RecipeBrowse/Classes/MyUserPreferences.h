//
//  MyUserPreferences.h
//  RecipeBrowse
//
//  Created by Bradley Miller on 8/19/10.
//  Copyright 2010 Luther College. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MyUserPreferences : UIViewController {
	IBOutlet UIView *preferenceView;
	IBOutlet UITextField *userField;
	IBOutlet UITextField *passField;
}


-(IBAction) saveUserPrefs;

@property	(nonatomic,retain) IBOutlet UITextField *userField;
@property	(nonatomic,retain) IBOutlet UITextField *passField;

@end
