//
//  Recipe.h
//  RecipeParse
//
//  Created by Bradley Miller on 8/31/10.
//  Copyright 2010 Luther College. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Recipe : NSObject {
	NSString *title;
	NSArray *ingredients;
	NSArray *method;
	NSString *servings;
	NSString *source;
	NSString *level;
	NSString *time;
	NSString *rating;
	NSArray *tags;
	NSArray *notes;
}

-(id) initWithString:(NSString *)recipeText;
-(NSString *) htmlIngredients;
-(NSString *) htmlMethod;
-(NSString *) htmlNotes;
-(NSString *) htmlInfo;

-(NSString *) htmlStringFromRecipe;

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *servings;
@property (nonatomic, retain) NSString *source;
@property (nonatomic, retain) NSString *level;
@property (nonatomic, retain) NSString *time;
@property (nonatomic, retain) NSString *rating;
@property (nonatomic, retain) NSArray *ingredients;
@property (nonatomic, retain) NSArray *method;
@property (nonatomic, retain) NSArray *tags;
@property (nonatomic, retain) NSArray *notes;

@end
