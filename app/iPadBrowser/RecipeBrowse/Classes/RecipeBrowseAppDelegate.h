//
//  RecipeBrowseAppDelegate.h
//  RecipeBrowse
//
//  Created by Bradley Miller on 7/14/10.
//  Copyright Luther College 2010. All rights reserved.
//

#import <UIKit/UIKit.h>


@class RecipeViewController;
@class DetailViewController;

@interface RecipeBrowseAppDelegate : NSObject <UIApplicationDelegate> {
    
    UIWindow *window;
    
    UISplitViewController *splitViewController;
    
    RecipeViewController *recipeViewController;
    DetailViewController *detailViewController;

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet UISplitViewController *splitViewController;
@property (nonatomic, retain) IBOutlet RecipeViewController *recipeViewController;
@property (nonatomic, retain) IBOutlet DetailViewController *detailViewController;

@end
