//
//  DetailViewController.m
//  RecipeBrowse
//
//  Created by Bradley Miller on 7/14/10.
//  Copyright Luther College 2010. All rights reserved.
//

#import "DetailViewController.h"
#import "MyUserPreferences.h"
#import "RecipeList.h"

@interface DetailViewController ()

- (void)configureView;
@end



@implementation DetailViewController

@synthesize toolbar, popoverController, detailItem, detailDescriptionLabel;
@synthesize recipeDetail;
@synthesize preferenceView;
@synthesize syncActInd;

#pragma mark -
#pragma mark Managing the detail item

/*
 When setting the detail item, update the view and dismiss the popover controller if it's showing.
 */
- (void)setDetailItem:(id)newDetailItem {
	// These two lines set the base URL for including images and CSS that I have as resources in the project.
	NSString *path = [[NSBundle mainBundle] bundlePath];
	NSURL *baseURL = [NSURL fileURLWithPath:path];
	
    if (detailItem != newDetailItem) {
        [detailItem release];
        detailItem = [newDetailItem retain];
        [self.recipeDetail loadHTMLString:detailItem baseURL:baseURL];
        // Update the view.
        [self configureView];
    }

    if (popoverController != nil) {
        [popoverController dismissPopoverAnimated:YES];
    }        
}


- (void)configureView {
    // Update the user interface for the detail item.
    detailDescriptionLabel.text = [detailItem description];   
}


#pragma mark -
#pragma mark Split view support

- (void)splitViewController: (UISplitViewController*)svc willHideViewController:(UIViewController *)aViewController withBarButtonItem:(UIBarButtonItem*)barButtonItem forPopoverController: (UIPopoverController*)pc {
    
    barButtonItem.title = @"Recipes";
    NSMutableArray *items = [[toolbar items] mutableCopy];
    [items insertObject:barButtonItem atIndex:0];
    [toolbar setItems:items animated:YES];
    [items release];
    self.popoverController = pc;
}


// Called when the view is shown again in the split view, invalidating the button and popover controller.
- (void)splitViewController: (UISplitViewController*)svc willShowViewController:(UIViewController *)aViewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem {
    
    NSMutableArray *items = [[toolbar items] mutableCopy];
    [items removeObjectAtIndex:0];
    [toolbar setItems:items animated:YES];
    [items release];
    self.popoverController = nil;
}


#pragma mark -
#pragma mark Rotation support

// Ensure that the view controller supports rotation and that the split view can therefore show in both portrait and landscape.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}


#pragma mark -
#pragma mark View lifecycle


 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *defaultObject = [NSUserDefaults standardUserDefaults];
    NSString *oldRecipe = [defaultObject objectForKey:@"last_recipe"];
    if (oldRecipe) {
        NSString *path = [[NSBundle mainBundle] bundlePath];
        NSURL *baseURL = [NSURL fileURLWithPath:path];
        [recipeDetail loadHTMLString:oldRecipe baseURL:baseURL];
    } else {
    [recipeDetail loadHTMLString:@"<html><body><h1 style=\"text-align: center;\">Welcome to Recipe Text</h1>" baseURL:nil];
    }
    
    RecipeList *shared = [RecipeList getSharedObject];
    NSLog(@"RecipeList = %@",shared);
}


/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/

- (void)viewDidUnload {
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.popoverController = nil;
}

- (IBAction)showPrefs {
	NSLog(@"+ button pressed");
	preferenceView.modalPresentationStyle = UIModalPresentationFormSheet;
	[self presentModalViewController:preferenceView animated:YES];

}

- (void)stopAnimation {
    [syncActInd stopAnimating];
}

- (IBAction)syncRecipes {
	RecipeList *myRecipes = [RecipeList getSharedObject];
    [syncActInd startAnimating];
	[myRecipes syncRecipes];
}

#pragma mark -
#pragma mark Memory management

/*
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
*/

- (void)dealloc {
    [popoverController release];
    [toolbar release];
    
    [detailItem release];
    [detailDescriptionLabel release];
    [super dealloc];
}

@end
