create table recipe (
   title varchar(50),
   google_key varchar(50) primary key,
   recipe_text text,
   last_update date,
   tags varchar(100)
);
