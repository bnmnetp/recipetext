//
//  RecipeList.h
//  RecipeBrowser
//
//  Created by Brad Miller on 2/8/10.
//  Copyright 2010 Luther College. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface RecipeList : NSObject {
	NSTableView *tableView;
	NSTextView *recTextView;
	
	NSArray *recipeList;
	NSFileManager *fm;
	
	NSSearchField *searchField;
	
	NSTextField *recipeTitle;
	
}

@property (nonatomic, retain) IBOutlet NSTableView *tableView;
@property (nonatomic, retain) IBOutlet NSTextView *recTextView;
@property (nonatomic, retain) IBOutlet NSSearchField *searchField;
@property (nonatomic, retain) IBOutlet NSTextField *recipeTitle;

@property (nonatomic, retain) NSArray *recipeList;

- (int)numberOfRowsInTableView:(NSTableView *)aTableView;
- (id)tableView:(NSTableView *)aTableView 
	objectValueForTableColumn:(NSTableColumn *)aTableColumn
			row:(int)rowIndex;
- (void)search:(id)sender;

@end
