//
//  main.m
//  RecipeBrowser
//
//  Created by Brad Miller on 2/8/10.
//  Copyright 2010 Luther College. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc,  (const char **) argv);
}
