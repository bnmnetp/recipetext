//
//  RecipeBrowserAppDelegate.h
//  RecipeBrowser
//
//  Created by Brad Miller on 2/8/10.
//  Copyright 2010 Luther College. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface RecipeBrowserAppDelegate : NSObject <NSApplicationDelegate> {
    NSWindow *window;
}

@property (assign) IBOutlet NSWindow *window;

@end
