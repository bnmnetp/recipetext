//
//  RecipeList.m
//  RecipeBrowser
//
//  Created by Brad Miller on 2/8/10.
//  Copyright 2010 Luther College. All rights reserved.
//

#import "RecipeList.h"

@implementation RecipeList

@synthesize recTextView;
@synthesize tableView;
@synthesize recipeList;
@synthesize searchField;
@synthesize recipeTitle;

- (id)init {
	[super init];
	NSArray *tempArray;
	fm = [NSFileManager defaultManager];
	tempArray = [fm contentsOfDirectoryAtPath:@"/Users/bmiller/Recipes" error:NULL];
	NSPredicate *endsWithTxt = [NSPredicate predicateWithFormat:@"self ENDSWITH[cd] %@", @".txt"];
	recipeList = [tempArray filteredArrayUsingPredicate:endsWithTxt];
	[recipeList retain];
	return self;
	
	
}

-(int)numberOfRowsInTableView:(NSTableView *)aTableView {
	return [recipeList count];
}

-(id)tableView:(NSTableView *)aTableView objectValueForTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex {
	NSString *v = [recipeList objectAtIndex:rowIndex];
	return v;
}

-(void)tableViewSelectionDidChange:(NSNotification *)notification {
	int row = [tableView selectedRow];
	if (row == -1) {
		return;
	}
	NSLog(@"selected %@", [recipeList objectAtIndex:row]);
	NSString *path = @"/Users/bmiller/Recipes/";
	path = [path stringByAppendingString:[recipeList objectAtIndex:row]];
	NSData *data = [NSData dataWithContentsOfFile:path];
	NSString *recipe = [NSString stringWithUTF8String:[data bytes]];
	NSString *rTitle = [recipeList objectAtIndex:row];
	[recipeTitle setStringValue:[rTitle substringToIndex:[rTitle length]-4]];
	[recTextView setString:recipe];
	
}

- (void)controlTextDidChange:(NSNotification *)obj
{
	NSTextView* textView = [[obj userInfo] objectForKey:@"NSFieldEditor"];
	
//    if (!completePosting && !commandHandling)	// prevent calling "complete" too often
//	{
//        completePosting = YES;
//        [textView complete:nil];
//        completePosting = NO;
//    }
	NSLog(@"Got a string");
	NSLog([textView string]);
	// set am searching to true


}

- (void)textDidChange:(NSNotification *)obj {
	NSLog(@"text changed");
}

- (void)search:(id)sender {

	NSString *searchFor = [searchField stringValue];
	NSMutableArray *searchResults = [[NSMutableArray alloc] init];
    NSEnumerator *e = [recipeList objectEnumerator];
	id object;
	
	// Split search string on whitespace(s)
	NSArray *searchWordList = [searchFor componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	
    // Create an array of predicates, one for each word in the list.
	// if there is more than one word in the search then create a compound
	// predicate to search for all the words.
	NSMutableArray *predArray = [[NSMutableArray alloc] init];
	NSPredicate *containPred;
	for (id myWord in searchWordList) {
		[predArray addObject:[NSPredicate predicateWithFormat:@"SELF contains[cd] %@", myWord]];
	}
	if ([predArray count] > 1) {
		containPred = [NSCompoundPredicate	andPredicateWithSubpredicates:predArray];
	} else {
		containPred = [predArray objectAtIndex:0];
	}


    BOOL match;
	NSString *recString;
	NSString *path = @"/Users/bmiller/Recipes/";
	if (!searchFor || [searchFor isEqualToString:@""]) {
		fm = [NSFileManager defaultManager];
		searchResults = [NSMutableArray arrayWithArray:  [fm contentsOfDirectoryAtPath:@"/Users/bmiller/Recipes" error:NULL]];
		NSPredicate *endsWithTxt = [NSPredicate predicateWithFormat:@"self ENDSWITH[cd] %@", @".txt"];
		searchResults = [NSMutableArray arrayWithArray:[searchResults filteredArrayUsingPredicate:endsWithTxt]];

	} else {
		
		while (object = [e nextObject]) {
			path = @"/Users/bmiller/Recipes/";
			path = [path stringByAppendingString:object];
			recString = [NSString stringWithContentsOfFile:path
											  encoding:NSUTF8StringEncoding
											     error: nil];
			match = [containPred evaluateWithObject:recString];
			if (match) {
				[searchResults addObject:object];
			}
		}
	}

	[searchResults retain];
	[recipeList release];
	recipeList = searchResults;
	[tableView reloadData];
	//NSLog(recString);
	
}

@end
