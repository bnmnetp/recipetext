#!/usr/bin/python

# Python template 1.0 by Peter Hosey
# 2007-08-02

#######################
# EDIT THIS PART HERE #
#######################

"""
Usage: filter-service <input
(Change the name of the service, and delete this line.)

This service recalibrates the framistan using the geeblefritzer.
(Write your own description, and replace the description above with the description you created, and delete this line.)
"""
from parseOne import Recipe
import codecs
import Growl

def main(input_text):
	# Write your documentation above, and implement your service here.

	# This statement, if left alone and unchanged, simply ignores the input. Thus, this service does nothing.
	myRecipe = Recipe(input_text)
	if myRecipe.name:
		f = codecs.open('/Users/bmiller/Recipes/import/'+myRecipe.name+'.txt','w','utf-8')
		name = myRecipe.name
	else:
		f = codecs.open('/Users/bmiller/Recipes/import/untitled.txt','w','utf-8')
		name = 'untitled'
	try:
		ur = unicode(myRecipe)
		f.write(ur)
		f.close()
		success = True
	except:
		success = False

	growlName = "Recipe Import"
	notifications = ["added to collection", "not added"]
	notifier = Growl.GrowlNotifier(growlName, notifications)
	notifier.register()

	if success:
	    notifier.notify("added to collection",
			    "Recipe Import",
			    "Added: " + name + " to ~/Recipes/import")
	else:
	    notifier.notify("not added",
			    "Recipe Import",
			    "Unable to add " + name)


################################
# OK, YOU CAN STOP EDITING NOW #
################################

if __name__ == "__main__":
	import sys

	if sys.argv[1:] == ['--help']:
		sys.exit(__doc__.strip())

	import codecs

	# Read the input.
	input_data = sys.stdin.read()
	try:
		# If it's UTF-8, this converts it to a proper Unicode string.
		input_text = codecs.utf_8_decode(input_data)[0]
	except UnicodeDecodeError:
		# Apparently, it's not UTF-8. We'll give you the raw bytes instead.
		input_text = input_data

	# Here, we pass the input to your function.
	main(input_text)
