import urllib
from googleappenginelogin import *
import pickle
import os,os.path
import plistlib
import time
from datetime import datetime


#APPSERVER = "http://recipepad.appspot.com"
APPSERVER = "http://localhost:8083"
#USER="bonelake@gmail.com"
#USER="test@example.com"
USER="59203846"

# get date information from pickle file

try:
    pfile = open(".rfile.db",'r')
    fileDict = pickle.load(pfile)
    pfile.close()
except:
    fileDict = {}

if not fileDict:
    lastsync = '2000-01-01 12:00:00'
else:
    lastsync = fileDict['lastsync']

lastsync = urllib.urlencode({'lastsync':lastsync})
#gae = GoogleAppEngineLogin(USER,PW,APPSERVER,"recipepad")

def updateTextFile(key, title, mtime):
    rhandle = urllib.urlopen(APPSERVER+"/api/"+USER+"/getrecipe/"+key)
    rtext = rhandle.read()
    title = title.strip()
    f = open(title+'.txt','w')
    f.write(rtext)
    f.close()
    mtimestamp = time.mktime(mtime.timetuple())
    os.utime(title+'.txt',(mtimestamp,mtimestamp))


f = urllib.urlopen(APPSERVER+"/api/"+USER+"/sync?"+lastsync)
# for compatibility with ObjectiveC results come back as a plist
syncList = plistlib.readPlist(f)
print "len of syncList = ", len(syncList)
i = 0
while i < len(syncList):
    key = syncList[i]
    title = syncList[i+1].strip()
    mtime = datetime.strptime(syncList[i+2],"%Y-%m-%d %H:%M:%S.%f")

    if key in fileDict:
        last_update = fileDict[key][1]
        if mtime > last_update:
            updateTextFile(key,title,mtime)
            fileDict[key] = (title,mtime)
    else:
        updateTextFile(key,title,mtime)
        fileDict[key] = (title,mtime)
    
    i += 3

fileDict['lastsync'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
dbFile = open(".rfile.db","w")
pickle.dump(fileDict,dbFile)
dbFile.close()
    
# Converting a datetime object to a timestamp:  time.mktime(d.timetuple())
#  datetime.fromtimestamp(xxxx) to convert to a datetime object
