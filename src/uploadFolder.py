#!/usr/bin/python2.6

import urllib, os.path, sys
import urllib2
import cookielib

user_id = "59203846"
recipe_dir = "/Users/bmiller/tmp/synctest/"
recipeList = os.listdir(recipe_dir)
for r in recipeList:
    print r
    if r[-4:] == '.txt':
        fname = recipe_dir + r

        params = {}
        params['filename'] = fname
        params['import_text'] = open(fname,'r').read()
        data = urllib.urlencode(params)
        f = urllib2.Request("http://recipepad.appspot.com/api/"+user_id+"/putrecipe",data)
        serv_resp = urllib2.urlopen(f)
        print serv_resp.read()

# Local Variables:
# py-which-shell: "python2.6" 
# End:
