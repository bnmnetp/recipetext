#!/usr/bin/python

# Convert all .txt files to .html using markdown
# Generate homepage with tag cloud
import os, os.path, glob
import codecs
import recipe_dissect
import markdown
import urllib
import django.utils.http

from recipe_dissect import ParsedRecipe

class RBook(object):
    """
    """

    def __init__(self, r_source_dir, site_dir):
        """
        
        Arguments:
        - `r_source_dir`:
        """
        self.r_source_dir = r_source_dir
        self.site_dir = site_dir
        
        self.tags = {}

        self.r_list = glob.glob(os.path.join(self.r_source_dir,'*.txt'))
        self.r_list.sort()
        
    def update_tags(self,rec):
        """
        
        Arguments:
        - `self`:
        - `rec`:
        """

        tlist = rec.tags.split(',')
        tlist = [x.strip() for x in tlist]
        for t in tlist:
            if t in self.tags:
                self.tags[t].append(rec.name)
            else:
                self.tags[t] = [rec.name]
        
    def gen_html(self,rec):
        """

        Arguments:
        - `self`:
        - `rec`:
        """
#        return markdown.markdown(unicode(rec))        
        return rec.toHTML()
    
    def write_html_file(self,html,rec):
        hfile = codecs.open(os.path.join(self.site_dir,rec.name+u'.html'),'w','utf-8')
        hfile.write(html)
        hfile.close()
    
    def create_tag_page(self,tag,page_list):
        """
        
        Arguments:
        - `self`:
        - `tag`:
        - `page_list`:
        """
        tpage = codecs.open(os.path.join(self.site_dir,tag+'.html'),'w','utf-8')

        hstring =  u'''
            <html>
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <link rel="stylesheet" href="recstyle.css" type="text/css" media="screen" />
            </head>
            <body>
            <div id="heading"
            <h1>%s</h1>
            </div>
            <div id="leftcolumn">
            <ul>
            ''' % (tag)
        tpage.write(hstring)
        
        for p in page_list:
            p.encode('utf-8')
            url = ""
            try:
                url = django.utils.http.urlquote(p)
            except:
                print "Error encoding url for",p
            tpage.write(u"<li> <a href=%s.html>%s</a>\n"% (url,p))
        tpage.write(u"</ul></div></body></html>")
        
        tpage.close()

    def create_index_page(self):
        ipage = codecs.open(os.path.join(self.site_dir,'index.html'),'w','utf-8')        
        hstring =  u'''
            <html>
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <link rel="stylesheet" href="recstyle.css" type="text/css" media="screen" />
            </head>
            <body>
            <div id="heading">
            <h1>My Recipes</h1>
            </div>
            '''
        ipage.write(hstring)


        ipage.write(u'<div id="leftcolumn"><h1>List of Recipes</h1>')
        ipage.write(u'<div id="recipe_section"><ul>')        
        for t in self.r_list:
            try:
                name = os.path.basename(t)
                name = name.replace('.txt','.html')
                url = django.utils.http.urlquote(name)
                name = name.replace('.html','')
                ipage.write(u"<li> <a href=%s>%s</a>\n"% (url,name))
            except:
                print "error encoding ", name
                name = django.utils.http.urlquote(name)
                ipage.write(u"<li> <a href=%s>%s</a>\n"% (url,name))

        ipage.write(u"</ul></div></div>")

        # tag section
        ipage.write(u'''
            <div id="tag_section">
            <h1>Tags</h1>
            <ul>
            ''')

        mint = 9999
        maxt = 0
        for t in self.tags:
            currLen = len(self.tags[t])
            if currLen < mint:
                mint = currLen
            if currLen > maxt:
                maxt = currLen
        dist = (maxt-mint) / 3

        tlist = self.tags.keys()
        tlist.sort()
        for t in tlist:
            currLen = len(self.tags[t])
            if currLen == mint:
                tagclass='smallestTag'
            elif currLen == maxt:
                tagclass='largestTag'
            elif currLen > (mint + dist*2):
                tagclass='largeTag'
            elif currLen > (mint + dist):
                tagclass='mediumTag'
            else:
                tagclass='smallTag'
            ipage.write(u'<li class="%s"> <a href="%s.html">%s</a>'%(tagclass,t,t))

        ipage.write(u"</body></html>")
        ipage.write(u'</ul></div>')        

    
    def create_web(self):
        for r in self.r_list:
            rfile = codecs.open(r,'r','utf-8')
            rtext = rfile.read()
            rec = ParsedRecipe(rtext)

    
            self.update_tags(rec)

            html = self.gen_html(rec)
            self.write_html_file(html,rec)

        for t in self.tags:
            self.create_tag_page(t, self.tags[t])

        self.create_index_page()
            


if __name__ == '__main__':
    RDIR='/Users/bmiller/Recipes'
    SDIR='/Users/bmiller/Sites/Recipes'
    myBook = RBook(RDIR,SDIR)
    myBook.create_web()
