#!/usr/bin/python
import sys


rf = open(sys.argv[1],'r')

line = rf.readline()
while line:
    if line.find("===") == 0:
        title = rf.readline()
        nf = open(title[:-1].replace("/","_")+'.txt','w')
        nf.write('# ' + title)
        line = rf.readline()
        while line.find("===") < 0 and line:
            if line[:-1] == "INGREDIENTS:" or line[:-1] == "METHOD:":
                nf.write("## "+line)
            else:
                nf.write(line)
            line = rf.readline()
        nf.close()
