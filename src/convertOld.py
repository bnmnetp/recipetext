import os, glob, os.path, codecs
from recipe_dissect import ParsedRecipe

for infile in glob.glob( os.path.join('/Users/bmiller/Recipes','*.txt') ):
    print infile
    rf = codecs.open(infile,'r','utf-8')
    recipeText = rf.read()
    rf.close()
    myRecipe = ParsedRecipe(recipeText)
    bname = os.path.basename(infile)
    rf = codecs.open('/Users/bmiller/Recipes/new/'+bname,'w','utf-8')
    rf.write(unicode(myRecipe))
