#!/usr/bin/python

from recipe_dissect import ImportedRecipe
import subprocess
import os.path
import Growl
import codecs

try:
    p = subprocess.Popen(['/usr/bin/pbpaste'],stdout=subprocess.PIPE)
    reader = codecs.getreader('utf-8')(p.stdout)
    recipeText = reader.read()
#    recipeText = p.stdout.read()
    myRecipe = ImportedRecipe(recipeText)
    success = True
except:
    print "error parsing recipe"
    success = False

path = '/Users/bmiller/Recipes/import/'
if myRecipe.name:
    name = myRecipe.name
else:
    name = 'untitled'
suff = '.txt'
num = 1

while os.path.exists(path+name+suff):
    name = name[:8] + '-' + str(num)
    num += 1

try:
    rf = codecs.open(path+name+suff,'w','utf-8')
    rf.write(unicode(myRecipe))
    rf.close()
    success = True
except:
    print "error saving recipe"
    success = False

growlName = "Recipe Import"
notifications = ["added to collection", "not added"]
notifier = Growl.GrowlNotifier(growlName, notifications)
notifier.register()

if success:
    notifier.notify("added to collection",
                    "Recipe Import",
                    "Added: " + name + " to ~/Recipes/import")
else:
    notifier.notify("not added",
                    "Recipe Import",
                    "Unable to add " + name)

