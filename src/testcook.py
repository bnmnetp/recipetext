from md5 import *
import urllib2
import cookielib

# ck = cookielib.Cookie(version=0, name='Name', value='1', port=None, port_specified=False, domain='www.example.com', domain_specified=False, domain_initial_dot=False, path='/', path_specified=True, secure=False, expires=None, discard=True, comment=None, comment_url=None, rest={'HttpOnly': None}, rfc2109=False

userid = md5("test@example.com").digest()
devCookie = cookielib.Cookie(version=0, name="dev_appserver_login",
                             value="%s-False-%s" % ("test@example.com","185804764220139124118"),
                             port="8081",
                             port_specified=True,
                             path="/",
                             path_specified=True,
                             domain="localhost",
                             domain_specified=True,
                             domain_initial_dot=False,
                             secure=False,
                             expires=None,
                             discard=False,
                             comment=None,
                             comment_url=None,
                             rest=None,
                             rfc2109=False)
print devCookie
cookiejar = cookielib.LWPCookieJar()
cookiejar.set_cookie(devCookie)
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookiejar))
urllib2.install_opener(opener)
try:
    opener.open("http://localhost:8081/")
except:
    print "fail"
cookiejar.set_cookie(devCookie)
opener.open("http://localhost:8081/")

# serv_req = urllib2.Request("http://localhost:8081/",None)
# x= urllib2.urlopen(serv_req)

